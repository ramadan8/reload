""" Various utility functions. """

from urllib.parse import urlparse

from youtube_dl import YoutubeDL

from .exceptions import MediaUnavailable

MEDIA_MODE = ["bestvideo", "bestaudio", "best"]


def validate_url(link: str) -> bool:
    """Make sure that a given URL's format is valid."""
    try:
        resp = urlparse(link)
        return all([resp.scheme, resp.netloc])
    except ValueError:
        return False


def download_media(link: str, mode: str) -> dict:
    """Get the data for a given link with the given mode."""
    opts = {
        "format": "best",
        "playlistend": MEDIA_MODE[int(mode) - 1],
        "noplaylist": True,
        "cachedir": False,
        "quiet": True,
    }

    with YoutubeDL(opts) as dl:
        resp = dl.extract_info(link, download=False)

        if not resp:
            raise MediaUnavailable()

        entry = resp["entries"][0] if resp.get("entries") else resp

        if entry and entry.get("url"):
            return entry

        raise MediaUnavailable()
