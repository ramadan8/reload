""" Various translations for each message. """

translations = {
    "en": {
        "invalid_link": ["❌", "Please send a valid link!"],
        "format_choose": ["🤔", "Please choose from the below formats."],
        "format_unavailable": ["😓", "The format you want isn't available..."],
        "media_unavailable": ["😓", "I couldn't find any media from that link..."],
        "media_found": [
            "😊",
            "Thanks for using our service\! Your media is available [at this link\!]({url})",
        ],
        "button_video_only": ["📷", "Video Only"],
        "button_audio_only": ["🎵", "Audio Only"],
        "button_both": ["🎥", "Both"],
    },
}


def _(key: str, locale: str = "en") -> str:
    emoji, text = translations[locale][key]

    return f"{emoji} {text}"
