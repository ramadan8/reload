""" Simple script to run the bot. """

import logging
import os
import sys
from typing import Optional

from telegram.ext import CallbackQueryHandler, Filters, MessageHandler, Updater

from .bot import button, echo

log = logging.getLogger(__name__)
logging.basicConfig(
    format="[%(asctime)s] [%(name)s] %(levelname)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.DEBUG,
)


def fetch_token() -> Optional[str]:
    """
    Try to find the bot's Telegram token in the environment variables and
    arguments.

    Tokens can be validated at the following endpoint, although we do not do
    that in this function as it will be done anyways later by the Python
    Telegram bot library.

    `https://api.telegram.org/bot{token}/getMe`

    It will return 200 if the token is valid.
    """
    if len(sys.argv) > 1:
        return sys.argv[1]

    token = os.getenv("TELEGRAM_TOKEN")

    if token:
        return token


def main() -> None:
    """Main entry-point for the script."""
    token = fetch_token()

    if not token:
        log.fatal(
            "No token could be found. Please provide one in the program's "
            "arguments or in the `TELEGRAM_TOKEN` environment variable."
        )

        quit(1)

    updater = Updater(token)

    dispatcher = updater.dispatcher

    dispatcher.add_handler(CallbackQueryHandler(button))
    dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
