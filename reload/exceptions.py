class BaseException(Exception):
    pass


class MediaUnavailable(BaseException):
    pass
