""" Callbacks for the Telegram bot. """

import logging

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode, Update
from telegram.ext import CallbackContext
from youtube_dl.utils import DownloadError

from .translate import _
from .exceptions import MediaUnavailable
from .util import download_media, validate_url

log = logging.getLogger(__name__)


def echo(update: Update, context: CallbackContext) -> None:
    """Start the downloader process."""
    if not validate_url(update.message.text):
        return update.message.reply_text(_("invalid_link"))

    context.user_data["media"] = update.message.text

    update.message.reply_text(
        _("format_choose"),
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(_("button_video_only"), callback_data="1"),
                    InlineKeyboardButton(_("button_audio_only"), callback_data="2"),
                ],
                [InlineKeyboardButton(_("button_both"), callback_data="3")],
            ]
        ),
    )

    log.debug("Started a conversation", extra={"user": update.effective_user.username})


def button(update: Update, context: CallbackContext) -> None:
    """
    After the user gives a link to the media they want to download, process what
    choice of format they want and give it to them as a link.
    """
    link = context.user_data.get("media")

    if not link:
        return

    query = update.callback_query
    query.answer()

    try:
        resp = download_media(link, query.data)
    except DownloadError:
        return query.edit_message_text(_("format_unavailable"))
    except MediaUnavailable:
        return query.edit_message_text(_("media_unavailable"))

    log.debug(
        f"Found media ({resp['extractor']}:{resp['id']})",
        extra={"user": update.effective_user.username},
    )

    return query.edit_message_text(
        _("media_found").format(url=resp["url"]),
        parse_mode=ParseMode.MARKDOWN_V2,
    )
